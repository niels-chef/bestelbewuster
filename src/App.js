import React, { Component } from 'react';
import ReactGA from 'react-ga';
import { Route, BrowserRouter as Router, Link } from "react-router-dom";
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import grey from '@material-ui/core/colors/grey'

import logoWhite from './images/logo_wit.png';
import logoColor from './images/logo.png';
import HamburgerWhite from './images/hamburger_white.png';
import HamburgerGreen from './images/hamburger_green.png';

import HomePage from  './containers/HomePage';
import CreateEvent from  './containers/CreateEvent';

import './App.scss';

const theme = createMuiTheme({
  palette: {
    primary: grey,
  },
});

class App extends Component {
  constructor() {
    super();
    this.state = {
      headerWhite: false,
      windowWidth: window.innerWidth,
      navOpen: false,
    }
  }
  componentDidMount() {
      window.addEventListener('resize', this.handleResize.bind(this));
    this.initializeReactGA();
    window.addEventListener('scroll', this.listenScrollEvent);
  }
  componentWillUnmount() {
    window.removeEventListener('resize', this.handleResize.bind(this));
  }
  handleResize() {
    this.setState({ windowWidth: window.innerWidth });
  }
  listenScrollEvent = e => {
    if (window.scrollY > 100) {
      this.setState({headerWhite: true})
    } else {
      this.setState({headerWhite: false })
    }
  }
  initializeReactGA() {
    ReactGA.initialize('UA-131189638-1');
    ReactGA.pageview('/');
  }
  render() {
    const {  headerWhite, navOpen, windowWidth } = this.state;
    const mobile = windowWidth < 768;
    return (
      <Router>
      <MuiThemeProvider theme={theme}>
        <div className="App">
          <div onClick={() => this.setState({ navOpen: false })} className={`navpanel ${navOpen ? 'navpanel--open' : ''}`}>
          <Link to="/" className="mobile-header-item">Homepage</Link>
            <Link to="/meeting-aanmaken" className="mobile-header-item">Probeer het zelf</Link>
          </div>
          <header className="App-header" style={{ backgroundColor: `${headerWhite ? 'white' : ''}`}}>
            <div className="bar">
              <div className="left">
                <Link to="/"><img className="header-logo" src={headerWhite ? logoColor : logoWhite} alt="bestelbewuster wit logo" /></Link>
                <div className="text-logo" style={{ color: `${headerWhite ? '#333' : 'white'}`}}>BestelBewuster</div>
              </div>
              {mobile ? (
                <div className={`right ${headerWhite ? 'right--active' : ''}`}>
                  <img onClick={() => this.setState({ navOpen: !navOpen})} className="hamburger-icon" src={headerWhite ? HamburgerGreen : HamburgerWhite} alt="mobile icon" />
                </div>
              ) : (
                <div className={`right ${headerWhite ? 'right--active' : ''}`}>
                  <Link to="/meeting-aanmaken" className="bar-right">Probeer het zelf</Link>
                </div>
              )}
            </div>
          </header>
            <Route path="/meeting-aanmaken" component={CreateEvent} />
            <Route path="/" exact component={HomePage} mobile={mobile} />
        </div>
        </MuiThemeProvider>
      </Router>
    );
  }
}

export default App;
