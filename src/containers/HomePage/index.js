import React, { Component } from 'react';

import './styles.scss';
import meeting from '../../images/meeting.png';
import logoColor from '../../images/logo.png';
import DenHaagLogo from '../../images/den_haag_logo.jpg';
import Award from '../../images/award.png';

class HomePage extends Component {
  constructor() {
    super();
    this.state = {
      contactInput: '',
      contactRequestSent: false,
    }
    this.sendContactDetails = this.sendContactDetails.bind(this)
  }
  sendContactDetails(email) {
    const body = {
      email
    }
    fetch('http://lunchplanner-api.eu-central-1.elasticbeanstalk.com/ contact-form', {
      method: 'post',
      body: JSON.stringify(body)
    }).then(res => {
      if (res.status === 200) {
        this.setState({ contactRequestSent: true })
      }
    });
    this.setState({ contactInput: ''})
  }
  render() {
    const { contactInput, contactRequestSent } = this.state;
    const { mobile } = this.props;
    return (
      <div className="App">
        <div className="jumbotron">

          <div className="jumbo-content">
            <div className="h1">BestelBewuster</div>
            <div className="sub-title">De web-based applicatie voor het efficiënt en bewust bestellen van zakelijke lunches</div>
          </div>
          <img src={meeting} className="meeting" alt="lunchmeeting"/>
        </div>
        <div className="container">
          <div className="usps">
            <div className="usp">
              <div className="usp-title">30%</div>
              <div className="usp-subtitle">30% van al het voedsel wordt tijdens lunchmeetings verspild</div>
            </div>
            <div className="usp">
              <div className="usp-title">8%</div>
              <div className="usp-subtitle">8% van alle CO<sub>2</sub> uitstoot wordt veroorzaakt door voedselverspilling</div>
            </div>
            <div className="usp">
              <div className="usp-title">70%</div>
              <div className="usp-subtitle">BestelBewuster kan tot wel 70% van de voedselverspilling tijdens lunchmeetings voorkomen</div>
            </div>
          </div>
        </div>
        <div className="problem">
          <div className="problem-content">
            <div className="h1">Het probleem</div>
            <p>31,6 % van het bestelde voedsel tijdens lunchmeetings wordt verspild, onder andere door no-shows, gebrek aan flexibiliteit en om goed over te komen.</p>
            <p>Bestelbewuster.nl gaat de strijd aan met het voedsel wat tijdens lunchmeetings verspild wordt. Dit doen wij door het proces efficiënt en eenvoudig te maken.
              Via BestelBewuster worden lunches besteld via een web-based applicatie. Deze is te integreren met ieder e-mailsysteem.</p>
            <p>Na het aanmaken ontvangen alle genodigden een e-mail. Omdat elk individu zelf op een bewuste manier zijn of haar lunch kiest, wordt nooit te veel besteld. Bewust ook, omdat je bij je keuze wordt geïnformeerd over de CO<sub>2</sub> footprint en de klimaatschade van jouw bestelling.
            En als je je afmeldt voor de vergadering wordt je lunch automatisch geannuleerd.</p>
          </div>
        </div>
        <div className="container">
          <div className="more-info">
            <div className="h2">Meer weten?</div>
            <div className="contact">
              <input className="contact-input" value={contactInput} placeholder="email" type="text" onChange={(e) => this.setState({ contactInput: e.target.value })} />
              <div
                className="contact-button"
                onClick={() => this.sendContactDetails(contactInput)}
                style={{background: `${contactRequestSent ? '#1e8a7b' : 'linear-gradient(45deg, #1e8a7b, #f5ed7f)'}`}}
                >
                  {contactRequestSent ? 'Verzonden' : 'Aanmelden'}
                </div>
            </div>
          </div>
          <div className="divider"></div>
          <div>
            <div className="third">
              <img className="the-hague-logo" src={DenHaagLogo} alt="logo gemeente Den Haag" />
            </div>
            <div className="twothird">
              “Met BestelBewuster.nl weet je wat je eet, qua product, prijs en impact. Een slim bestelsysteem gekoppeld aan bewustwording, aldus de jury. De kosten zijn inzichtelijk en er is een professioneel team om de klus te klaren.”
            </div>
          </div>
          <div className="awards">
            <div className="half text-center">
              <img className="award-image" src={Award} alt="award icon" />
              <div className="award-title">Winnaar Startup-In-Residence Den Haag</div>
              Urban Challenge ‘No Lunch Wasted’
            </div>
            <div className="half text-center">
              <img className="award-image" src={Award} alt="award icon" />
              <div className="award-title">Winnaar Startup-In-Residence Amsterdam</div>
              SIR AMA wildcard
            </div>
          </div>
        </div>
        <div id="footer" className="footer">
          <div className="footer-items">
            {!mobile && <div className="fourth hide-mobile"></div>}
            <div className="fourth"><img className="header-logo" src={logoColor} alt="bestelbewuster wit logo" /></div>
            <div className="fourth">info@bestelbewuster.nl</div>
            {!mobile && <div className="fourth hide-mobile"></div>}
          </div>
          <div className="copyright">© {(new Date()).getFullYear()} BestelBewuster, Alle rechten voorbehouden</div>
        </div>
      </div>
    );
  }
}

export default HomePage;
