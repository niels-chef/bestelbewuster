import React from 'react';
import axios from "axios";
import TextField from '@material-ui/core/TextField';
import './styles.scss';

export class CreateEvent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      emailInput: '',
      formSent: false,
      error: undefined,
    }
    this.createEvent = this.createEvent.bind(this);
  }
  createEvent() {
    if (!/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(this.state.emailInput)) {
      return alert('Voer een geldig email-adres in')
    }
    axios.post('http://lunchplanner-api.eu-central-1.elasticbeanstalk.com/events/', {
      email: this.state.emailInput
    })
    .then((response) => {
      if (response.status === 200) {
        this.setState({ formSent: true, error: undefined })
      } else {
        this.setState({ error: 'Er is helaas iets misgegaan. Probeer het later opnieuw'})
      }
    })
    .catch((error) => {
      this.setState({ error: 'Er is helaas iets misgegaan. Probeer het later opnieuw'})
    });
    this.setState({ contactInput: ''})
  }
  render() {
    const { emailInput, formSent, error } = this.state;
    const steps = [
      'Creeer hier gemakkelijk een lunch afspraak, zoals je dat ook in bijvoorbeeld Outlook of Gmail doet.',
      'Accepteer de uitnodiging, zodat de organisator en ons systeem weet dat je aanwezig zal zìjn',
      'Zodra je op aanwezig staat, ontvang je direct een email van ons.',
      'Stel je lunch samen uit het beschikbare menu.',
      'Het BestelBewuster systeem verwerkt de bestelling en zorgt dat de organisator op de hoogte is.',
    ]
    return (
      <div className="create-event">
        <div className="container">
          <h1>Maak een meeting event aan</h1>
          <p>Test hier het eenvoudige proces van BestelBewuster.</p>
          <div className="steps-container">
          {steps.map((step, index) => (
            <div className="step-container">
              <div className="step-number">{index + 1}</div>
              <div className="step-text">{step}</div>
            </div>
          ))}
          </div>
          <TextField
           id="email"
           label="Email"
           value={emailInput}
           onChange={(e) => this.setState({ emailInput: e.target.value })}
           margin="normal"
         />
          <div className="event-submit" onClick={() => this.createEvent()}>{formSent ? 'Verzonden' : 'Maak event aan'}</div>
          <div className="error-message">{error}</div>
        </div>
      </div>
    )
  }
}


export default CreateEvent
